

# Laravel Web on Docker

You can run Laravel Web on Docker by Step By Step

1. Install **Docker**.
2. **Goto command line to directory laravel-app:** 
    ```bash
    $ cd laravel-app
    ```
3. **Run web:** 
    ```bash
    $ docker-compose up -d
    ```
4. **Checking docker container running (Services: app, webserver, db should be running):** 
    ```bash
    $ docker ps
    ```
5. Go to website at **http:\\\localhost** on host machine browser
6. **Stop docker with command**: 
    ```bash
    $ docker-compose down
    ```
---

# Run unit test

You can run Laravel Unittest on Docker by Step By Step

1. Run docker-compose
2. $ docker-compose exec app ./vendor/bin/phpunit [--filter 'method-name']

---

# Run unit test with Selinium

## Install laravel Dusk
- Run these commands below:
    ```ruby
    $ composer require --dev laravel/dusk
    $ php artisan dusk:install
    ```

## Setup config:
- Add Selenium at **services** section in docker-composer.yml file:
    
    ```yml
    #Selenium
    selenium:
        image: selenium/standalone-chrome:3.11.0-antimony
        container_name: selenium
        restart: unless-stopped
        tty: true
        ports:
            - "4444:4444"
        volumes:
            - /dev/shm:/dev/shm
        networks:
            - app-network  
        links:
            - app:app.dev
    ```

- Add these configs to .env file:
    ```ini
    USE_DOCKER_SELENIUM=true
    DOCKER_APP_URL=http://webserver
    ```
    
    > http://webserver ( docker's host url )

- Update these config in .env file:

    ```ini
    APP_ENV=testing
    ```
- Update file DuskTestCase content:

    ```python
    /**
    * Prepare for Dusk test execution.
    *
    * @beforeClass
    * @return void
    */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    protected function baseUrl()
    {
        return env('USE_SELENIUM') ? env('DOCKER_APP_URL') : env('APP_URL');
    }

    /**
    * Create the RemoteWebDriver instance.
    *
    * @return \Facebook\WebDriver\Remote\RemoteWebDriver
    */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1920,1080',
        ]);

        if (env('USE_SELENIUM', 'false') == 'true') {
            return RemoteWebDriver::create(
                'http://selenium:4444/wd/hub', DesiredCapabilities::chrome());
        } else {
            return RemoteWebDriver::create(
                'http://localhost:9515', DesiredCapabilities::chrome());
        }
    }
    ```

## Selenium Test on host machine
- Change .env config **USE_DOCKER_SELENIUM**:
    ```ini
    USE_DOCKER_SELENIUM=false
    ```
- Run command
    ```bash
    $ php artisan dusk
    ```

## Selenium Test on docker
- Change .env config **USE_DOCKER_SELENIUM**:
    ```ini
    USE_DOCKER_SELENIUM=true
    ```
- Run command
    ```bash
    $ docker-compose exec app php artisan dusk
    ```
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    public $timestamps = true;

    protected $table = 'tasks';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'title', 'description',
    ];

}

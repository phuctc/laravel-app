<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{

    public function testRegisterEmptyName() {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', '')
                ->type('email', 'phuctc@imt-soft.com')
                ->type('password', '12345678')
                ->type('password_confirmation', '12345678')
                ->press('Register')
                ->assertSee(\Lang::get('validation.required', ['attribute' => 'name']))
                ->pause(500);
        });
    }

    public function testRegisterEmptyEmail() {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Phuc Trinh')
                ->type('email', '')
                ->type('password', '12345678')
                ->type('password_confirmation', '12345678')
                ->press('Register')
                ->assertSee(\Lang::get('validation.required', ['attribute' => 'email']))
                ->pause(500);
        });
    }

    public function testRegisterEmailInvalidFormat() {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Phuc Trinh')
                ->type('email', 'phuctc')
                ->type('password', '12345678')
                ->type('password_confirmation', '12345678')
                ->press('Register')
                ->assertSee(\Lang::get('validation.email', ['attribute' => 'email']))
                ->pause(500);
        });
    }
    
    public function testRegisterEmailHasExist()
    {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Test User')
                ->type('email', 'testdusk2@imt-soft.com')
                ->type('password', '12345678')
                ->type('password_confirmation', '12345678')
                ->press('Register')
                ->assertSee(\Lang::get('validation.unique', ['attribute' => 'email']))
                ->pause(500);
        });
    }

    public function testRegisterEmptyPassword()
    {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Phuc Trinh')
                ->type('email', 'phuctc@imt-soft.com')
                ->type('password', '')
                ->type('password_confirmation', '12345678')
                ->press('Register')
                ->assertSee(\Lang::get('validation.required', ['attribute' => 'password']))
                ->pause(500);
        });
    }

    public function testRegisterNotMatchConfirmationPassword()
    {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Phuc Trinh')
                ->type('email', 'phuctc@imt-soft.com')
                ->type('password', '12345678')
                ->type('password_confirmation', '')
                ->press('Register')
                ->assertSee(\Lang::get('validation.confirmed', ['attribute' => 'password']))
                ->pause(500);
        });
    }

    public function testRegisterPasswordNotEnoughChar()
    {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Phuc Trinh')
                ->type('email', 'phuctc@imt-soft.com')
                ->type('password', '12345')
                ->type('password_confirmation', '12345')
                ->press('Register')
                ->assertSee(\Lang::get('validation.min.string', ['attribute' => 'password', 'min' => '8']))
                ->pause(500);
        });
    }

    public function testRegisterSuccess()
    {
        $this->browse(function ($browser) {
            $browser->visit('register')
                ->type('name', 'Phuc Trinh')
                ->type('email', 'phuctc@imt-soft.com')
                ->type('password', '12345678')
                ->type('password_confirmation', '12345678')
                ->press('Register')
                ->assertPathIs('/home')
                ->pause(500);
        });
    }
}

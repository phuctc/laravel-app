<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{

    public function testLoginSuccess()
    {
        $this->browse(function ($browser) {
            $browser->visit('login')
                ->type('email', 'phuctc@imt-soft.com')
                ->type('password', '12345678')
                ->press('Login')
                ->assertPathIs('/home')
                ->pause(500);
        });
    }
}
